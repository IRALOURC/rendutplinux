*# TP1 : (re)Familiaration avec un système GNU/Linux

## 0. Préparation de la machine

### 🌞 Setup de deux machines Rocky Linux configurées de façon basique.

#### un accès internet (via la carte NAT)

node1.tp1.b2 : 
```
$ ip a 

2: enp0s3: <BROADCAST,MULTICAST,UP,LOWERUP> mtu 1500 qdisc fqcodel state UP group default qlen 1000
    link/ether 08:00:27:88:2c:2f brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       validlft 85735sec preferredlft 85735sec
    inet6 fe80::a00:27ff:fe88:2c2f/64 scope link noprefixroute 
       validlft forever preferredlft forever
```
```
$ ip r s

default via 10.0.2.2 dev enp0s3 proto dhcp metric 100 
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100 
10.101.1.0/24 dev enp0s8 proto kernel scope link src 10.101.1.11 metric 101
```
node2.tp1.b2 : 
```
$ ip a

2: enp0s3: <BROADCAST,MULTICAST,UP,LOWERUP> mtu 1500 qdisc fqcodel state UP group default qlen 1000
    link/ether 08:00:27:e0:f7:f6 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       validlft 85494sec preferredlft 85494sec
    inet6 fe80::a00:27ff:fee0:f7f6/64 scope link noprefixroute 
       validlft forever preferredlft forever
```
```
$ ip r s

default via 10.0.2.2 dev enp0s3 proto dhcp metric 100 
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100 
10.101.1.0/24 dev enp0s8 proto kernel scope link src 10.101.1.12 metric 101 
```
un accès à un réseau local : 

node1.tp1.b2 : 
```
$ ip a 

3: enp0s8: <BROADCAST,MULTICAST,UP,LOWERUP> mtu 1500 qdisc fqcodel state UP group default qlen 1000
    link/ether 08:00:27:ec:c0:b1 brd ff:ff:ff:ff:ff:ff
    inet 10.101.1.11/24 brd 10.101.1.255 scope global noprefixroute enp0s8
       validlft forever preferredlft forever
    inet6 fe80::a00:27ff:feec:c0b1/64 scope link 
       validlft forever preferredlft forever
```
node2.tp1.b2 : 
```
$ ip a 

3: enp0s8: <BROADCAST,MULTICAST,UP,LOWERUP> mtu 1500 qdisc fqcodel state UP group default qlen 1000
    link/ether 08:00:27:4f:1d:d0 brd ff:ff:ff:ff:ff:ff
    inet 10.101.1.12/24 brd 10.101.1.255 scope global noprefixroute enp0s8
       validlft forever preferredlft forever
    inet6 fe80::a00:27ff:fe4f:1dd0/64 scope link 
       validlft forever preferredlft forever
```

les machines doivent avoir un nom : 

node1.tp1.b2 : 
```
$ hostname
node1.tp1.b2
```
node1.tp1.b2 : 
```
$ hostname
node2.tp1.b2
```
utiliser 1.1.1.1 comme serveur DNS : 

node1.tp1.b2 : 
```
$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8

AME=enp0s8
DEVICE=enp0s8

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.101.1.11
NETMASK=255.255.255.0

DNS11=1.1.1.1
```
```
$ dig ynov.com

;; SERVER: 192.168.1.1#53(192.168.1.1)
```
node2.tp1.b2:
```
$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8

NAME=enp0s8
DEVICE=enp0s8

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.101.1.12
NETMASK=255.255.255.0

DNS1=1.1.1.1
```
```
$ dig ynov.com

;; SERVER: 192.168.1.1#53(192.168.1.1)
```
les machines doivent pouvoir se joindre par leurs noms respectifs : 

node1 : 
```
$ cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.101.1.12 node2

$ ping node2
PING node2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node2 (10.101.1.12): icmpseq=1 ttl=64 time=1.37 ms
64 bytes from node2 (10.101.1.12): icmpseq=2 ttl=64 time=0.767 ms
^C
--- node2 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 0.767/1.067/1.368/0.302 ms
```
node 2 : 
```
$ cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.101.1.11 node1

$ ping node1
PING node1 (10.101.1.12) 56(84) bytes of data.
64 bytes from node1 (10.101.1.12): icmpseq=1 ttl=64 time=0.082 ms
64 bytes from node1 (10.101.1.12): icmpseq=2 ttl=64 time=0.065 ms
64 bytes from node1 (10.101.1.12): icmpseq=3 ttl=64 time=0.065 ms
^C
--- node1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2036ms
rtt min/avg/max/mdev = 0.065/0.070/0.082/0.012 ms
```

## I. Utilisateurs : 

### 1. Création et configuration : 
```
$ useradd user2 -m -s /bin/sh -u 2000

$ groupadd user2

$ sudo usermod -aG admins user 2
```
```
$ sudo visudo /etc/sudoers

## Allows people in group wheel to run all commands
%wheel	ALL=(ALL)	ALL
%admin  ALL=(ALL)       ALL

```
node1 :
```
$ groups
admin wheel
```
node2 : 
```
$ groups
admin wheel
```

## 2. SSH

sur le terminal du poste d'administration : 
```
$ ssh-keygen -t rsa -b 4096
```
j'enregriste la cle ssh dans mon dossier /.ssh/idrsa
```
$ cat /Users/claireiralour/.ssh/idrsa
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAACmFlczI1Ni1jdHIAAAAGYmNyeXB0AAAAGAAAABBchgHSG7
apPMFf7wnrrg0cAAAAEAAAAAEAAAIXAAAAB3NzaC1yc2EAAAADAQABAAA...
```
dans ma vm : 
```
$ cd .ssh
$ vim authorizedkeys
$ ssh-copy-id
```
```
$ ssh admin@10.101.1.11 
Activate the web console with: systemctl enable --now cockpit.socket
[admin@node1 ~]$
```

## II. Partitionnement : 

### 1. Préparation de la VM
```
$ lsblk 
NAME                        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda                           8:0    0    8G  0 disk 
├─sda1                        8:1    0    1G  0 part /boot
└─sda2                        8:2    0    7G  0 part 
  ├─rlbastion--ovh1fr-root 253:0    0  6,2G  0 lvm  /
  └─rlbastion--ovh1fr-swap 253:1    0  820M  0 lvm  [SWAP]
sdb                           8:16   0    3G  0 disk 
sdc                           8:32   0    3G  0 disk 
```

### 2. Partitionnement : 

🌞 Utilisez LVM : 
```
$ sudo pvcreate /dev/sdb
  Physical volume "/dev/sdb" successfully created.
  ```
  ```
$ sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.
```
```
$ sudo pvs
  PV         VG                Fmt  Attr PSize  PFree
  /dev/sda2  rlbastion-ovh1fr lvm2 a--  <7,00g    0 
  /dev/sdb                     lvm2 ---   3,00g 3,00g
  /dev/sdc                     lvm2 ---   3,00g 3,00g
  ```
  ```
  $ sudo vgcreate disques /dev/sdb
  Volume group "disques" successfully created
  ```
  ```
  $ sudo vgextend disques /dev/sdc
  Volume group "disques" successfully extended
  ```
  ```
  $ sudo lvs
  LV   VG                Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  lv1  disques           -wi-a-----   1,00g                                                    
  lv2  disques           -wi-a-----   1,00g                                                    
  lv3  disques           -wi-a-----   1,00g                                                    
  root rlbastion-ovh1fr -wi-ao----  <6,20g                                                    
  swap rlbastion-ovh1fr -wi-ao---- 820,00m          
  ```
  ```
  $ sudo mkfs -t ext4 /dev/disques/lv1
mke2fs 1.45.6 (20-Mar-2020)
En train de créer un système de fichiers avec 262144 4k blocs et 65536 i-noeuds.
UUID de système de fichiers=df81289b-3375-40a8-91a5-8ced722766f6
Superblocs de secours stockés sur les blocs : 
	32768, 98304, 163840, 229376

Allocation des tables de groupe : complété                        
Écriture des tables d'i-noeuds : complété                        
Création du journal (8192 blocs) : complété
Écriture des superblocs et de l'information de comptabilité du système de
fichiers : complété
```
```
$ sudo mkfs -t ext4 /dev/disques/lv2
mke2fs 1.45.6 (20-Mar-2020)
En train de créer un système de fichiers avec 262144 4k blocs et 65536 i-noeuds.
UUID de système de fichiers=6f2e9a43-59eb-45f3-b031-a48c11ce2d56
Superblocs de secours stockés sur les blocs : 
	32768, 98304, 163840, 229376

Allocation des tables de groupe : complété                        
Écriture des tables d'i-noeuds : complété                        
Création du journal (8192 blocs) : complété
Écriture des superblocs et de l'information de comptabilité du système de
fichiers : complété
```
```
$ sudo mkfs -t ext4 /dev/disques/lv3
mke2fs 1.45.6 (20-Mar-2020)
En train de créer un système de fichiers avec 262144 4k blocs et 65536 i-noeuds.
UUID de système de fichiers=55abda30-16d5-4ee4-9970-fb62f5fb9ad7
Superblocs de secours stockés sur les blocs : 
	32768, 98304, 163840, 229376

Allocation des tables de groupe : complété                        
Écriture des tables d'i-noeuds : complété                        
Création du journal (8192 blocs) : complété
Écriture des superblocs et de l'information de comptabilité du système de
fichiers : complété
```
```
$ df -h
Sys. de fichiers                    Taille Utilisé Dispo Uti% Monté sur
devtmpfs                              891M       0  891M   0% /dev
tmpfs                                 909M       0  909M   0% /dev/shm
tmpfs                                 909M    8,6M  901M   1% /run
tmpfs                                 909M       0  909M   0% /sys/fs/cgroup
/dev/mapper/rlbastion--ovh1fr-root   6,2G    2,0G  4,2G  33% /
/dev/sda1                            1014M    240M  775M  24% /boot
tmpfs                                 182M       0  182M   0% /run/user/1000
/dev/mapper/disques-lv1               976M    2,6M  907M   1% /mnt/disque1
/dev/mapper/disques-lv2               976M    2,6M  907M   1% /mnt/disque2
/dev/mapper/disques-lv3               976M    2,6M  907M   1% /mnt/disque
```
🌞 Grâce au fichier /etc/fstab, faites en sorte que cette partition soit montée automatiquement au démarrage du système.
```
$ sudo cat /etc/fstab
[sudo] Mot de passe de admin : 

#
# /etc/fstab
# Created by anaconda on Tue Sep 28 13:23:22 2021
#
# Accessible filesystems, by reference, are maintained under '/dev/disk/'.
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
#
# After editing this file, run 'systemctl daemon-reload' to update systemd
# units generated from this file.
#
/dev/mapper/rlbastion--ovh1fr-root /                       xfs     defaults        0 0
UUID=a1e28f4d-c1f8-40f0-8f79-2b1e8242742b /boot                   xfs     defaults        0 0
/dev/mapper/rlbastion--ovh1fr-swap none                    swap    defaults        0 0
/dev/disques/lv1 /mnt/disque1	ext4	defaults 0 0
/dev/disques/lv2 /mnt/disque2	ext4	defaults 0 0
/dev/disques/lv3 /mnt/disque3	ext4	defaults 0 0
```
## 1. Interaction avec un service existant : 

🌞 Assurez-vous que : 
```
$ systemctl is-active firewalld
active
```
```
$ systemctl is-enabled firewalld
enabled
```

## 2. Création de service

### A. Unité simpliste

🌞 Créer un fichier qui définit une unité de service web.service dans le répertoire /etc/systemd/system.

```
$ sudo nano /etc/systemd/system/web.service
```
```
$ sudo firewall-cmd --add-port=8888/tcp
success
```
```$ sudo systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
```
```
$ sudo systemctl start  web
```
```
$ sudo systemctl enable  web
Created symlink /etc/systemd/system/multi-user.target.wants/web.service → /etc/systemd/system/web.service.
```
🌞Une fois le service démarré, assurez-vous que pouvez accéder au serveur web : avec un navigateur ou la commande curl sur l'IP de la VM, port 8888.

```
$ sudo systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; enabled; vendor preset: dis>
   Active: active (running) since Thu 2021-09-30 16:09:54 CEST; 6min ago
 Main PID: 23687 (python3)
    Tasks: 1 (limit: 11398)
   Memory: 9.8M
   CGroup: /system.slice/web.service
           └─23687 /bin/python3 -m http.server 8888

sept. 30 16:09:54 node1.tp1.b2 systemd[1]: Started Very simple web service.
```
## B. Modification de l'unité

🌞 Créer un utilisateur web.
```
$ sudo useradd web
```
🌞 Modifiez l'unité de service web.service créée précédemment en ajoutant les clauses :
```
$ sudo cat /etc/systemd/system/web.service 
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888
User=web
WorkingDirectory=/srv/service

[Install]
WantedBy=multi-user.target
```

🌞 Placer un fichier de votre choix dans le dossier créé dans /srv et tester que vous pouvez y accéder une fois le service actif. Il faudra que le dossier et le fichier qu'il contient appartiennent à l'utilisateur web.
```
$ sudo nano /srv/service/test1
```
```
$ sudo systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; enabled; vendor preset: dis>
   Active: active (running) since Thu 2021-09-30 16:49:08 CEST; 17s ago
 Main PID: 23810 (python3)
    Tasks: 1 (limit: 11398)
   Memory: 9.4M
   CGroup: /system.slice/web.service
           └─23810 /bin/python3 -m http.server 8888

sept. 30 16:49:08 node1.tp1.b2 systemd[1]: web.service: Succeeded.
sept. 30 16:49:08 node1.tp1.b2 systemd[1]: Stopped Very simple web service.
sept. 30 16:49:08 node1.tp1.b2 systemd[1]: Started Very simple web service.
```
🌞 Vérifier le bon fonctionnement avec une commande curl
```
$ curl localhost:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
```
