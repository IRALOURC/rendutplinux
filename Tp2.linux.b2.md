# TP2 pt. 1 : Gestion de service

# I. Un premier serveur web

## 1. Installation

🖥️ **VM web.tp2.linux**

| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80          | ?             |
| `web.tp2.linux` | `10.102.1.11` | Service ssh             | 22          | ?             |

🌞 **Installer le serveur Apache**
```
sudo dnf install -y @httpd
```
```
$ vim /etc/httpd/conf/httpd.conf

:g/^ *#.*/d
:wq
```
🌞 **Démarrer le service Apache**
```
$ sudo systemctl enable --now httpd -permanent
```
```
$  systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor prese>
   Active: active (running) since Wed 2021-10-06 12:09:56 CEST; 46min ago
     Docs: man:httpd.service(8)
 Main PID: 820 (httpd)
   Status: "Running, listening on: port 443, port 80"
    Tasks: 213 (limit: 11398)
   Memory: 29.8M
   CGroup: /system.slice/httpd.service
           ├─820 /usr/sbin/httpd -DFOREGROUND
           ├─847 /usr/sbin/httpd -DFOREGROUND
           ├─848 /usr/sbin/httpd -DFOREGROUND
           ├─849 /usr/sbin/httpd -DFOREGROUND
           └─850 /usr/sbin/httpd -DFOREGROUND

oct. 06 12:09:55 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
oct. 06 12:09:56 web.tp2.linux httpd[820]: AH00558: httpd: Could not reliably d>
oct. 06 12:09:56 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
oct. 06 12:09:56 web.tp2.linux httpd[820]: Server configured, listening on: por>
```
```
$ sudo ss -alnpt
State     Recv-Q    Send-Q       Local Address:Port        Peer Address:Port    Process                                                                         
LISTEN    0         128                0.0.0.0:22               0.0.0.0:*        users:(("sshd",pid=817,fd=5))                                                  
LISTEN    0         128                      *:80                     *:*        users:(("httpd",pid=850,fd=4),("httpd",pid=849,fd=4),("httpd",pid=848,fd=4),("httpd",pid=820,fd=4))
LISTEN    0         128                   [::]:22                  [::]:*        users:(("sshd",pid=817,fd=7))                                                  
LISTEN    0         128                      *:443                    *:*        users:(("httpd",pid=850,fd=9),("httpd",pid=849,fd=9),("httpd",pid=848,fd=9),("httpd",pid=820,fd=9))
```

🌞 **TEST**
- vérifier que le service est démarré
```
$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor prese>
   Active: active (running) since Wed 2021-10-06 12:09:56 CEST; 53min ago
   ```
   - vérifier qu'il est configuré pour démarrer automatiquement
```
$ sudo systemctl enable --now httpd
```
```
$ sudo systemctl is-enabled httpd
enabled
```
- vérifier avec une commande curl localhost que vous joignez votre serveur web localement
```
$ curl localhost:443
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>400 Bad Request</title>
</head><body>
<h1>Bad Request</h1>
<p>Your browser sent a request that this server could not understand.<br />
Reason: You're speaking plain HTTP to an SSL-enabled server port.<br />
 Instead use the HTTPS scheme to access this URL, please.<br />
</p>
</body></html>
```
```
$ curl localhost:80
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
...
```

## 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**

- affichez le contenu du fichier httpd.service qui contient la définition du service Apache
```
$ cat /usr/lib/systemd/system/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#	[Service]
#	Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

🌞 **Déterminer sous quel utilisateur tourne le processus Apache**

- mettez en évidence la ligne dans le fichier de conf principal d'Apache (httpd.conf) qui définit quel user est utilisé
```
$ cat /etc/httpd/conf/httpd.conf | grep -i user
User apache
    LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
      LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
```
- utilisez la commande `ps -ef` pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf
```
$ ps -ef
UID          PID    PPID  C STIME TTY          TIME CMD
root           1       0  0 15:40 ?        00:00:02 /usr/lib/systemd/systemd --s
apache       873     846  0 15:40 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       874     846  0 15:40 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       875     846  0 15:40 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       876     846  0 15:40 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
admin       1809    1673  0 16:06 pts/0    00:00:00 ps -ef
```
- la page d'accueil d'Apache se trouve dans `/usr/share/testpage/`
  - vérifiez avec un `ls -al` que tout son contenu est **accessible** à l'utilisateur mentionné dans le fichier de conf
```
$ ls -al /usr/share/testpage
total 12
drwxr-xr-x.  2 root root   24  6 oct.  11:25 .
drwxr-xr-x. 90 root root 4096  6 oct.  12:24 ..
-rw-r--r--.  1 root root 7621 11 juin  17:23 index.html
```

🌞 **Changer l'utilisateur utilisé par Apache**

- créez le nouvel utilisateur
  ```
  $ cd /usr/share/httpd
  ```
  ```
  $ sudo useradd Usera
  ```
- modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur
```
$ sudo nano /etc/httpd/conf/httpd.conf 
User Usera
Group Usera
```
je relance ma vm pour les modifications
- utilisez une commande `ps` pour vérifier que le changement a pris effet
```
$ ps -ef
UID          PID    PPID  C STIME TTY          TIME CMD
root           1       0  1 16:29 ?        00:00:02 /usr/lib/systemd/systemd --s
Usera        872     842  0 16:29 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
Usera        873     842  0 16:29 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
Usera        874     842  0 16:29 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
Usera        875     842  0 16:29 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```

🌞 **Faites en sorte que Apache tourne sur un autre port**

- modifiez la configuration d'Apache pour lui demande d'écouter sur un autre port de votre choix 
```
$ sudo nano /etc/httpd/conf/httpd.conf

Listen 3600
```
- ouvrez un nouveau port firewall, et fermez l'ancien
```
$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
$ sudo firewall-cmd --remove-port=22/tcp --permanent
success
$ sudo firewall-cmd --add-port=3600/tcp --permanent
success
$ sudo firewall-cmd --reload
success
$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources: 
  services: cockpit dhcpv6-client ssh
  ports: 3600/tcp

```
- prouvez avec une commande ss que Apache tourne bien sur le nouveau port choisi
```
$ sudo ss -alnpt
[sudo] Mot de passe de admin : 
State     Recv-Q    Send-Q       Local Address:Port        Peer Address:Port    Process                                                                         
LISTEN    0         128                0.0.0.0:22               0.0.0.0:*        users:(("sshd",pid=820,fd=5))                                                  
LISTEN    0         128                   [::]:22                  [::]:*        users:(("sshd",pid=820,fd=7))                    
```
erreur sur la configuration des ports

# II. Une stack web plus avancée

⚠⚠⚠ **Réinitialiser votre conf Apache avant de continuer** ⚠⚠⚠  
En particulier :

- reprendre le port par défaut
- reprendre l'utilisateur par défaut

## 1. Intro

> Pas mal de blabla nécessaire avant de vous lancer. Lisez bien la partie en entier.

Le serveur web `web.tp2.linux` sera le serveur qui accueillera les clients. C'est sur son IP que les clients devront aller pour visiter le site web.  

La serveur de base de données `db.tp2.linux` sera un serveur uniquement accessible depuis `web.tp2.linux`. Les clients ne pourront pas y accéder. Le serveur de base de données stocke les infos nécessaires au serveur web, pour le bon fonctionnement du site web.

---

Bon j'ai un peu réfléchi et le but pour nous là c'est juste d'avoir un serv web + une db, peu importe ce que c'est le site. J'ai pas envie d'aller deep dans la conf de l'un ou de l'autre avec vous pour le moment. Donc on va installer un truc un peu clé en main : Nextcloud.

En plus c'est utile comme truc : c'est un p'tit serveur pour héberger ses fichiers via une WebUI, style Google Drive. Mais on l'héberge nous-mêmes :)

Il y a [**une doc officielle Rocky Linux** plutôt bien fichue pour l'install de Nextcloud](https://docs.rockylinux.org/guides/cms/cloud_server_using_nextcloud/#next-steps), je vous laisse la suivre.

⚠️⚠️⚠️**ATTENTION** lisez bien toute la suite avant de vous lancer dans l'install, **lisez la partie en entier.** Ca vous évitera bien des soucis. ⚠️⚠️⚠️

Dans la doc officielle, on vous fait installer le serveur web (Apache) et la base de données (MariaDB) sur la même machine. **Ce n'est PAS ce que nous voulons : nous voulons avoir chaque service sur une machine dédiée.** Donc vous allez devoir ajuster la doc un petit peu. Rien de bien violent :

- quand on vous parle du serveur web, vous faites ça sur `web.tp2.linux`
- quand on vous parle de la base de données, vous faites ça sur `db.tp2.linux`
- à la fin, quand c'est en place, on vous demande d'aller l'interface Web de nextcloud et d'y saisir l'IP de la base de données, pour que NextCloud puisse s'y connecter. Vous saisirez ici l'IP de `db.tp2.linux` à la place de `localhost`

---
---

**Aussi** dans la doc vous est fourni un lien pour installer MariaDB de façon secure. C'est parfait, suivez-le.  
**Par contre** on ne vous donnez aucune infos sur quelle base de données créer dans le serveur de base de données.  
**Donc** avant de démarrer NextCloud, vous devrez :

- démarrer le service de base de données sur `db.tp2.linux`
- vous connecter au serveur de base de données
- exécutez des commandes SQL pour configurer la base, qu'elle soit utilisable par NextCloud

---
---

**Enfin, quelques tips en vrac pour que vous dérouliez l'install dans de bonnes conditions.** Certains tips vont vous paraître random. Beaucoup moins une fois que vous aurez déroulé la doc :

➜ ***Lisez*** et ***suivez bien toutes les instructions***. Toutes les étapes sont strictement nécessaires.

➜ Vous pouvez récupérer votre **timezone** plus facilement en une simple commande : `timedatectl`.

➜ **Si on vous parle d'un dossier et qu'il n'existe pas, créez-le.** Si vous ne savez quelles permissions lui donner, donnez-lui les mêmes permissions que les autres fichiers/dossiers qui se trouvent dans le même dossier.

➜ **Une fois que vous aurez fini d'installer NextCloud**, vous devez visiter l'interface Web. Vous ouvrirez donc votre navigateur, et vous rendrez à l'URL `http://web.tp2.linux`. La page d'accueil de NextCloud s'affichera. **NE VOUS CONNECTEZ PAS** et passez à l'installation de la base de données. C'est sur cet écran que vous indiquerez à NextCloud comment se connecter à votre base, une fois que vous l'aurez installé.

➜ Dans la doc, il est dit : "As noted earlier, we are using the ***"Apache Sites Enabled"*** procedure found here to configure Apache"

Cette ***"Apache Sites Enabled" procedure*** fait référence à une façon d'organiser le dossier `/etc/httpd` pour pas que ce soit le bordel :

- on a créé cette façon de faire car Apache est souvent utilisé pour héberger plusieurs sites web en même temps
- l'idée c'est de faire
  - un dossier `sites-available/` qui contient un fichier de configuration par site web
  - un dossier `sites-enabled/` qui contient des liens vers les fichiers de `sites-available`
- de cette façon il est plus simple de s'y retrouver :
  - un fichier par site, c'est clean
  - un dossier dédié à la conf des sites, et pas à la conf d'Apache plus générale, c'est clean
  - si on veut mettre un site hors-ligne sans faire de la crasse (commenter des lignes, supprimer la conf liée au site etc.) c'est EZ on a juste a supprimer le lien dans `sites-enabled/` ce qui garde intact le vrai fichier de conf dans `sites-available/`
- bref c'est clean quoi :)

**SAUF QUE** si vous suivez juste la doc, ça va pas fonctionner. En effet, les fichiers dans `sites-enabled/` ne seront jamais lus par défaut. Vous devez donc ajouter la ligne suivante dans le fichier `/etc/httpd/httpd.conf` (tout en bas) :

```bash
IncludeOptional sites-enabled/*
```

## 2. Setup

🖥️ **VM db.tp2.linux**

| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | ?           | ?             |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | ?           | ?             |

> Ce tableau devra figurer à la fin du rendu, avec les ? remplacés par la bonne valeur (un seul tableau à la fin). Je vous le remets à chaque fois, à des fins de clarté, pour lister les machines qu'on a à chaque instant du TP.

### A. Serveur Web et NextCloud

**Créez les 2 machines et déroulez la [📝**checklist**📝](#checklist).**

🌞 Install du serveur Web et de NextCloud sur `web.tp2.linux`

- n'oubliez pas de réinitialiser votre conf Apache avant de continuer
  - remettez le port et le user par défaut en particulier
- déroulez [la doc d'install de Rocky](https://docs.rockylinux.org/guides/cms/cloud_server_using_nextcloud/#next-steps)
  - **uniquement pour le serveur Web + NextCloud**, vous ferez la base de données MariaDB après
  - quand ils parlent de la base de données, juste vous sautez l'étape, on le fait après :)
- je veux dans le rendu **toutes** les commandes réalisées
  - n'oubliez pas la commande `history` qui permet de voir toutes les commandes tapées précédemment

Une fois que vous avez la page d'accueil de NextCloud sous les yeux avec votre navigateur Web, **NE VOUS CONNECTEZ PAS** et continuez le TP

📁 **Fichier `/etc/httpd/conf/httpd.conf`** (que vous pouvez renommer si besoin, vu que c'est le même nom que le dernier fichier demandé)
📁 **Fichier `/etc/httpd/conf/sites-available/web.tp2.linux`**

### B. Base de données

🌞 **Install de MariaDB sur `db.tp2.linux`**

- déroulez [la doc d'install de Rocky](https://docs.rockylinux.org/guides/database/database_mariadb-server/)
- manipulation 
- je veux dans le rendu **toutes** les commandes réalisées
- vous repérerez le port utilisé par MariaDB avec une commande `ss` exécutée sur `db.tp2.linux`

🌞 **Préparation de la base pour NextCloud**

- une fois en place, il va falloir préparer une base de données pour NextCloud :
  - connectez-vous à la base de données à l'aide de la commande `sudo mysql -u root`
  - exécutez les commandes SQL suivantes :

```sql
# Création d'un utilisateur dans la base, avec un mot de passe
# L'adresse IP correspond à l'adresse IP depuis laquelle viendra les connexions. Cela permet de restreindre les IPs autorisées à se connecter.
# Dans notre cas, c'est l'IP de web.tp2.linux
# "meow" c'est le mot de passe :D
CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'meow';

# Création de la base de donnée qui sera utilisée par NextCloud
CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

# On donne tous les droits à l'utilisateur nextcloud sur toutes les tables de la base qu'on vient de créer
GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';

# Actualisation des privilèges
FLUSH PRIVILEGES;
```

> Par défaut, vous avez le droit de vous connectez localement à la base si vous êtes `root`. C'est pour ça que `sudo mysql -u root` fonctionne, sans nous demander de mot de passe. Evidemment, n'importe quelles autres conditions ne permettent pas une connexion aussi facile à la base.

🌞 **Exploration de la base de données**

- afin de tester le bon fonctionnement de la base de données, vous allez essayer de vous connecter, comme NextCloud le fera :
  - depuis la machine `web.tp2.linux` vers l'IP de `db.tp2.linux`
  - vous pouvez utiliser la commande `mysql` pour vous connecter à une base de données depuis la ligne de commande
    - par exemple `mysql -u <USER> -h <IP_DATABASE> -p`
- utilisez les commandes SQL fournies ci-dessous pour explorer la base

```sql
SHOW DATABASES;
USE <DATABASE_NAME>;
SHOW TABLES;
```

- trouver une commande qui permet de lister tous les utilisateurs de la base de données

> Les utilisateurs de la base de données sont différents des utilisateurs du système Linux sous-jacent. Les utilisateurs de la base définissent des identifiants utilisés pour se connecter à la base afin d'y voir ou d'y modifier des données.

### C. Finaliser l'installation de NextCloud

🌞 sur votre PC

- modifiez votre fichier `hosts` (oui, celui de votre PC, de votre hôte)
  - pour pouvoir joindre l'IP de la VM en utilisant le nom `web.tp2.linux`
- avec un navigateur, visitez NextCloud à l'URL `http://web.tp2.linux`
  - c'est possible grâce à la modification de votre fichier `hosts`
- on va vous demander un utilisateur et un mot de passe pour créer un compte admin
  - ne saisissez rien pour le moment
- cliquez sur "Storage & Database" juste en dessous
  - choisissez "MySQL/MariaDB"
  - saisissez les informations pour que NextCloud puisse se connecter avec votre base
- saisissez l'identifiant et le mot de passe admin que vous voulez, et validez l'installation

🌞 **Exploration de la base de données**

- connectez vous en ligne de commande à la base de données après l'installation terminée
- déterminer combien de tables ont été crées par NextCloud lors de la finalisation de l'installation
  - ***bonus points*** si la réponse à cette question est automatiquement donnée par une requête SQL


